terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region                  = "eu-central-1"
  shared_credentials_file = "./.aws/creds"
}

variable "do_token" {}
variable "pvt_key" {
  default = "~/.ssh/id_rsa"
}
