variable "lesson_name" {
  default = "service-discovery"
}

variable "servers_count" {
  default = 2
}

resource "aws_key_pair" "deployer" {
  key_name   = var.aws_key_name
  public_key = var.ssh_pub_key
}

resource "aws_instance" "servers" {
  count         = var.servers_count
  ami           = "ami-05f7491af5eef733a"
  instance_type = "t2.micro"

  tags = {
    Name = "${var.lesson_name}-${count.index + 1}"
  }

  key_name = aws_key_pair.deployer.key_name

  vpc_security_group_ids = [aws_security_group.public.id]
}

resource "aws_security_group" "public" {
    name        = "${var.lesson_name}_security_group"

    ingress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }

    egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }

    tags = {
      Name        = "${var.lesson_name}_private_network"
    }
}
