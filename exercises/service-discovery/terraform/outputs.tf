output "webserver_ips" {
  value = aws_instance.servers.*.public_ip
}
output "webserver_private_ips" {
  value = aws_instance.servers.*.private_ip
}
