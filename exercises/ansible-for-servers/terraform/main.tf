terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

data "digitalocean_ssh_key" "terraform" {
  for_each = toset(var.ssh_key_names)
  name = each.value
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

output "droplets" {
  value = digitalocean_droplet.web
}

# Create a web server
resource "digitalocean_droplet" "web" {
  count = var.droplet_count
  image = var.droplet_image
  name = format("${var.droplet_name}-%02.f", count.index + 1)
  region = var.droplet_region
  size = var.droplet_size
  ssh_keys = values(data.digitalocean_ssh_key.terraform)[*].id
}

resource "digitalocean_loadbalancer" "public" {
  name   = var.loadbalancer_name
  region = var.droplet_region

  forwarding_rule {
    entry_port     = var.entry_port
    entry_protocol = var.entry_protocol

    target_port     = var.target_port
    target_protocol = var.entry_protocol
  }

  healthcheck {
    port     = var.target_port
    protocol = var.entry_protocol
    path     = var.healthcheck_path
  }

  droplet_ids = digitalocean_droplet.web.*.id
}

resource "digitalocean_record" "www" {
  domain = var.domain_name
  type   = "A"
  name   = "www"
  value  = digitalocean_loadbalancer.public.ip
}
